---
title: "Nine PsyLinks"
slug: ninelinks
date: 2024-08-16
author: Roman
---

Recently I've been assembling some PsyLinks for anyone who is curious to try it out:

[![Photo of a stack of plastic bags with PCBs](/img/blog/2024-08-06_before.jpg)](/img/blog/2024-08-06_before.jpg)

It was a huge help that the SMD parts were already soldered on by [PCBWay](https://pcbway.com). These were partially sponsored by PCBWay, which I am very grateful for. The quality is perfect, couldn't have done it better myself.

You can't imagine how happy I am for not having to hand-solder the tiny SC-70 [voltage booster](https://www.ti.com/product/TPS61220) (2mm x 2mm) anymore! I also appreciate that PCBWay left no break-off points on the sides of the PCBs, since they tend to scratch the skin and I usually have to file them off manually.

I still can't outsource the assembly of through-hole components since I manually have to cut the connectors short enough so the bottom side doesn't scratch the skin. But I hope some day I can fully outsource the assembly and can focus on research and development instead.

[![Photo of the same PCBs in an assembled form, showing 9 assembled PsyLinks](/img/blog/2024-08-07_after.jpg)](/img/blog/2024-08-07_after.jpg)

Of the 9 PsyLinks on the photo, 4 are already reserved, but feel free to ask for one if you are interested!
