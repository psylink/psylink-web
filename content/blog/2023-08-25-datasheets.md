---
title: "Data Sheets"
slug: datasheets
date: 2023-08-25
author: Roman
---

After learning about the [4-document-model](https://documentation.divio.com/) ([permalink](https://archive.is/RsxqH)), an insightful guide to writing better documentation for software projects, I realized that PsyLink really suffers from having its documentation so spread-out among the dozens of pages with no clear reference page that contains it all.

As a first step to mitigate this, I wrote extensive data sheets for the power module and the electrode module, bundled right along with their source in the [schematics folder](https://codeberg.org/psylink/psylink/src/branch/master/schematics).  You can find them here:

- [Power Module](https://codeberg.org/psylink/psylink/src/branch/master/schematics/power_module4.2/README.md)
- [Electrode Module](https://codeberg.org/psylink/psylink/src/branch/master/schematics/electrode_module3.3/README.md)

Hopefully this will help you to get the most out of the boards and reduce the mistakes you do while replicating or using them!
